{
  description = "A tool to find and remove reddit watermarks.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      lib = pkgs.lib;
      poetry2nix = inputs.poetry2nix.lib.mkPoetry2Nix {inherit pkgs;};

      p2nix_defaults = {
        projectDir = ./.;
        python = pkgs.python312;
        overrides = poetry2nix.overrides.withDefaults (
          _: prev:
            lib.genAttrs [
              "ruff"
              "numpy"
              "pynvml"
              "opencv-python"
            ]
            (package: prev.${package}.override {preferWheel = true;})
        );
      };
    in {
      packages = rec {
        reddit_crop_tool = poetry2nix.mkPoetryApplication p2nix_defaults;
        default = reddit_crop_tool;
      };

      devShells = rec {
        reddit_crop_tool =
          (poetry2nix.mkPoetryEnv p2nix_defaults
            // {
              editablePackageSources = {
                reddit_crop_tool = ./reddit_crop_tool;
              };
            })
          .env
          .overrideAttrs (old: {
            buildInputs = with pkgs; [
              poetry
              just
              python312Packages.pudb
            ];
          });
        default = reddit_crop_tool;
      };

      apps = rec {
        reddit_crop_tool = {
          type = "app";
          program = "${self.packages.reddit_crop_tool}/bin/rct";
        };
        default = reddit_crop_tool;
      };
    });
}
