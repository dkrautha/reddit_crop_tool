import logging
import logging.config
from enum import Enum
from typing import Self


class LoggingVerbosity(Enum):
    STANDARD = 0
    VERBOSE = 1

    def to_logging_level(self: Self) -> str:
        if self == LoggingVerbosity.VERBOSE:
            return "DEBUG"
        return "INFO"


def setup_logging(verbosity: LoggingVerbosity) -> None:
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "simple": {
                    "format": "%(message)s",
                },
            },
            "handlers": {
                "stdout": {
                    "class": "logging.StreamHandler",
                    "stream": "ext://sys.stdout",
                    "formatter": "simple",
                },
            },
            "loggers": {
                "root": {
                    "level": verbosity.to_logging_level(),
                    "handlers": ["stdout"],
                },
            },
        },
    )


__all__ = ["LoggingVerbosity", "setup_logging"]
