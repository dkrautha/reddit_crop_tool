""""""

from __future__ import annotations

import logging
import multiprocessing
from dataclasses import astuple, dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Self

import click
import cv2
import numpy as np

from . import _logging

if TYPE_CHECKING:
    from cv2.typing import MatLike


@dataclass
class BoundingBox:
    x: int
    y: int
    w: int
    h: int

    def __str__(self: Self) -> str:
        return f"{self.x}, {self.y}, {self.w}, {self.h}"


@dataclass
class CroppableImage:
    filename: Path
    img: MatLike
    bounding_box: BoundingBox

    def crop(self: Self) -> MatLike:
        img = self.img
        bb = self.bounding_box
        x, y, w, h = astuple(bb)
        return img[y : y + h, x : x + w]


def get_bounding_box(img: MatLike) -> BoundingBox | None:
    original_h, original_w, alpha_type = img.shape

    not_transparent = 3
    transparent = 4

    if alpha_type == not_transparent:
        mask = np.array((36, 36, 36))
    elif alpha_type == transparent:
        mask = np.array((36, 36, 36, 255))
    else:
        return None

    gray = cv2.inRange(img, mask, mask)
    gray = cv2.bitwise_not(gray)
    contours, _ = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    largest = max(contours, key=cv2.contourArea)

    x, y, w, h = cv2.boundingRect(largest)
    if x == 0 and y == 0 and w == original_w and h == original_h:
        return None

    return BoundingBox(x, y, w, h)


def read_and_get_bounding_box(filename: Path) -> CroppableImage | None:
    img = cv2.imread(str(filename), cv2.IMREAD_UNCHANGED)
    if img is None:
        return None
    bb = get_bounding_box(img)
    return CroppableImage(filename, img, bb) if bb else None


def crop_and_save(img: CroppableImage) -> None:
    cropped = img.crop()
    new_name = f"{img.filename.with_suffix('')}_cropped{img.filename.suffix}"
    cv2.imwrite(new_name, cropped)


@click.command()
@click.argument("filenames", nargs=-1, required=True, type=Path)
@click.option("--perform_crop", "-c", is_flag=True, default=False)
@click.option("--verbose", "-v", is_flag=True, default=False)
@click.version_option(prog_name="reddit_crop_tool", version="0.1.0")
def main(
    *,
    filenames: list[Path],
    perform_crop: bool,
    verbose: bool,
) -> None:
    verbosity = _logging.LoggingVerbosity(verbose)

    _logging.setup_logging(verbosity)

    logger = logging.getLogger("reddit_crop_tool")

    logger.debug("Received filenames: %s", filenames)
    logger.debug("Perform crop: %s", perform_crop)
    logger.debug("Verbose: %s", verbose)

    with multiprocessing.Pool() as pool:
        results = pool.map(read_and_get_bounding_box, filenames)

    filtered = [r for r in results if r is not None]
    for img in filtered:
        logger.info("%s", img.filename)
    if perform_crop:
        with multiprocessing.Pool() as pool:
            pool.map(crop_and_save, filtered)


__all__ = []
